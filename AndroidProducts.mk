#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_tb128fu.mk

COMMON_LUNCH_CHOICES := \
    aosp_tb128fu-user \
    aosp_tb128fu-userdebug \
    aosp_tb128fu-eng
